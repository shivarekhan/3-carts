const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

// Q1. Find all the items with price more than $65.
const itemsWithPriceMoreThan65 = Object.entries(products[0]).filter(function (current) {
    if (current[1].quantity) {
        const price = Number(current[1].price.replace("$", ""));
        if (price > 65) {
            return current;
        }
    } else {
        const casr = current[1].filter(function (current) {
            const key = Object.keys(current)
            const price = Number(current[key].price.replace("$", ""));
            if (price > 65) {
                console.log(current);
                return current;
            }
        }, []);
    }

}, []);


// Q2. Find all the items where quantity ordered is more than 1.
const itemWithOrderMoreThan1 = Object.entries(products[0]).filter(function (current) {
    // const price = Number(current[1].price.replace("$",""));
    if (current[1].quantity >= 2) {
        const itemWithOrderMoreThan1 = Object.entries(products[0]).filter(function (current) {
            if (current[1].quantity) {
                // const price = Number(current[1].price.replace("$",""));
                if (current[1].quantity >= 2) {
                    return current;
                }
            } else {
                const casr = current[1].filter(function (current) {
                    const key = Object.keys(current)
                    const quantity = Number(current[key].quantity);
                    if (quantity >= 2) {
                        return current;
                    }
                }, []);
            }
        
        }, []);


//Q.3 Get all items which are mentioned as fragile.
const itemWhichAreFragile = Object.entries(products[0]).filter(function (current) {
    if (current[1].quantity) {
        // const price = Number(current[1].price.replace("$",""));
        if (current[1].type == 'fragile') {
            return current;
        }
    } else {
        const casr = current[1].filter(function (current) {
            const key = Object.keys(current)
            const isFragile = Number(current[key].type);
            if (isFragile == 'fragile') {
                return current;
            }
        }, []);
    }

}, []);